from sys import platform
import ctypes

from PyQt5 import QtCore, QtGui, QtWidgets, uic

from Contact import Contact
from ContactManager import ContactManager


class ContactListDialog(QtWidgets.QDialog):
    def __init__(self, contactlist, contactFile):
        super(ContactListDialog, self).__init__()
        uic.loadUi('UI/ContactListDialog.ui', self)
        self.setWindowTitle('All Contacts')
        self.contactFile = contactFile
        self.contactList = contactlist

        if platform == 'win32':
            appId = 'mailer'
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
                appId)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap('res/icon.ico'))
        self.setWindowIcon(icon)

        self.pushButtonAdd.clicked.connect(self.onClickAdd)
        self.pushButtonDelete.clicked.connect(self.onClickDelete)
        self.pushButtonEdit.clicked.connect(self.onClickEdit)
        self.pushButtonOk.clicked.connect(self.accept)
        self.pushButtonSelectAll.clicked.connect(self.onClickSelectAll)
        self.pushButtonDeselectAll.clicked.connect(self.onClickDeselectAll)
        self.pushButtonInvertSelection.clicked.connect(
            self.onClickInvertSelection)

        self.tableWidget.itemSelectionChanged.connect(self.updateButtonStatus)

        self.tableWidget.setColumnCount(3)
        self.tableWidget.setHorizontalHeaderLabels(
            ['First name', 'Last name', 'Email'])
        self.updateTable(self.contactList)

        if len(self.contactList) == 0 or len(self.tableWidget.selectedItems()) == 0:
            self.pushButtonDelete.setDisabled(True)
            self.pushButtonEdit.setDisabled(True)

        self.show()

    def onClickSelectAll(self):
        self.tableWidget.selectAll()

    def onClickDeselectAll(self):
        selectedItems = self.tableWidget.selectedItems()
        for item in selectedItems:
            item.setSelected(False)

    def onClickInvertSelection(self):
        selectedItems = self.tableWidget.selectedItems()
        self.tableWidget.selectAll()
        for item in selectedItems:
            item.setSelected(False)

    def onClickAdd(self):
        dialog = AddContactDialog(self.contactList, self.contactFile)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            contact = dialog.getContact()
            self.contactList += (contact, )
            self.updateTable(self.contactList)
            cMgr = ContactManager(self.contactFile)
            cMgr.saveContact(contact)

            self.updateButtonStatus

    def onClickDelete(self):
        contacts = self.getSelectedContacts()
        messageBox = QtWidgets.QMessageBox(
            QtWidgets.QMessageBox.Question, 'WARNING', '')
        messageBox.setStandardButtons(
            QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        messageBox.setDefaultButton(QtWidgets.QMessageBox.Yes)
        if len(contacts) == 1:
            messageBox.setText('Do you really want to delete this Contact?')
        else:
            messageBox.setText(
                'Do you really want to delete these ' + str(len(contacts)) + ' Contacts ?')

        info = ''
        for contact in contacts:
            info += contact.email + '\n'
        messageBox.setInformativeText(info)

        if messageBox.exec() == QtWidgets.QMessageBox.Yes:
            newList = ()
            for c in self.contactList:
                if not c in contacts:
                    newList += (c, )

            self.contactList = newList
            self.updateTable(self.contactList)

            cMgr = ContactManager(self.contactFile)
            cMgr.exportContacts(self.contactList)

            if len(self.contactList) == 0:
                self.pushButtonDelete.setDisabled(True)
                self.pushButtonEdit.setDisabled(True)
            self.updateButtonStatus()

    def getContactList(self):
        return self.contactList

    def onClickEdit(self):
        contact = self.getSelectedContacts()[0]
        if not contact == None:
            dialog = EditContactDialog(
                contact, self.contactList, self.contactFile)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                self.contactList = dialog.getContactList()
                self.updateTable(self.contactList)
                cMgr = ContactManager(self.contactFile)
                cMgr.exportContacts(self.contactList)

    def updateTable(self, contactlist):
        self.tableWidget.setRowCount(0)
        self.tableWidget.setRowCount(len(contactlist))
        n = 0
        for contact in contactlist:
            item = QtWidgets.QTableWidgetItem(contact.firstName)
            self.tableWidget.setItem(n, 0, item)
            item = QtWidgets.QTableWidgetItem(contact.lastName)
            self.tableWidget.setItem(n, 1, item)
            item = QtWidgets.QTableWidgetItem(contact.email)
            self.tableWidget.setItem(n, 2, item)
            n += 1

        self.tableWidget.sortItems(2)

        header = self.tableWidget.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)

    def updateButtonStatus(self):
        if len(self.tableWidget.selectedItems()) == 0:
            self.pushButtonDelete.setDisabled(True)
            self.pushButtonEdit.setDisabled(True)
        elif len(self.tableWidget.selectedItems()) > 3:
            self.pushButtonDelete.setDisabled(False)
            self.pushButtonEdit.setDisabled(True)
        else:
            self.pushButtonDelete.setDisabled(False)
            self.pushButtonEdit.setDisabled(False)

    def getSelectedContacts(self):
        row = self.tableWidget.selectedItems()
        if len(row) >= 3:
            items = self.tableWidget.selectedItems()
            emails = ()

            for c in items:
                if '@' in c.text():
                    emails += (c.text(), )

            contacts = ()
            for c in self.contactList:
                if c.email in emails:
                    contacts += (c, )
            return contacts


class AddContactDialog(QtWidgets.QDialog):
    def __init__(self, contactlist, contactFile):
        super(AddContactDialog, self).__init__()
        uic.loadUi('UI/ContactDialog.ui', self)
        self.setWindowTitle('Add a new Contact')
        self.contactFile = contactFile
        self.contactList = contactlist

        self.newContact = Contact()

        if platform == 'win32':
            appId = 'mailer'
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
                appId)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap('res/icon.ico'))
        self.setWindowIcon(icon)

        self.pushButtonOk.clicked.connect(self.addContact)
        self.pushButtonCancel.clicked.connect(self.reject)

    def addContact(self):
        if self.lineEditMail.text() == '':
            self.lineEditMail.setStyleSheet('background-color: #FFDDDD')
            return
        self.lineEditMail.setStyleSheet('background-color: #FFFFFF')
        fName = self.lineEditFirstName.text()
        lName = self.lineEditLastName.text()
        email = self.lineEditMail.text()
        exists = False
        for c in self.contactList:
            if c.email == email:
                exists = True
                break
        if not exists:
            self.newContact = Contact(fName, lName, email)
            self.done(1)
        else:
            self.lineEditMail.setStyleSheet('background-color: #FFDDDD')
            messageBox = QtWidgets.QMessageBox(
                QtWidgets.QMessageBox.Warning, '', 'This Email is allready in use!')
            messageBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
            messageBox.exec()

    def getContact(self):
        return self.newContact


class EditContactDialog(QtWidgets.QDialog):
    def __init__(self, contact, contactlist, contactFile):
        super(EditContactDialog, self).__init__()
        uic.loadUi('UI/ContactDialog.ui', self)
        self.setWindowTitle('Edit Contact ' + contact.name)
        self.contactFile = contactFile
        self.contactList = contactlist

        if platform == 'win32':
            appId = 'mailer'
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
                appId)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap('res/icon.ico'))
        self.setWindowIcon(icon)

        self.oldMail = contact.email

        self.lineEditFirstName.setText(contact.firstName)
        self.lineEditLastName.setText(contact.lastName)
        self.lineEditMail.setText(contact.email)

        self.pushButtonOk.clicked.connect(self.editContact)
        self.pushButtonCancel.clicked.connect(self.reject)

    def editContact(self):
        if self.lineEditMail.text() == '':
            self.lineEditMail.setStyleSheet('background-color: #FFDDDD')
            return
        self.lineEditMail.setStyleSheet('background-color: #FFFFFF')
        fName = self.lineEditFirstName.text()
        lName = self.lineEditLastName.text()
        email = self.lineEditMail.text()

        contact = Contact(fName, lName, email)
        exists = False
        for c in self.contactList:
            exists = True

        if exists and not self.oldMail == email:
            self.lineEditMail.setStyleSheet('background-color: #FFDDDD')
            messageBox = QtWidgets.QMessageBox(
                QtWidgets.QMessageBox.Warning, '', 'This Email is allready in use!')
            messageBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
            messageBox.exec()
        else:
            newList = ()
            for c in self.contactList:
                if c.email == self.oldMail:
                    newList += (contact, )
                    self.oldMail = ''
                else:
                    newList += (c, )
            self.contactList = newList
            self.done(1)

    def getContactList(self):
        return self.contactList
