# Sendgrid-API-Key:
# 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
#
# Key-Name:
# PYTHON_TEST_KEY
#
# Key-ID:
# RqyfiuNeTjyBbdeetorJvA


import os
import sys
import ctypes
import base64
from threading import Thread

from PyQt5 import QtCore, QtGui, QtWidgets, uic
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail, Attachment, FileContent, FileName, FileType, Disposition

# Own modules
import MailManager
from Contact import Contact
from ContactManager import ContactManager
from RecipientSelectDialog import RecipientSelectDialog
from PreferencesDialog import PreferencesDialog
import PreferencesManager as PreferencesManager
from ContactListDialog import ContactListDialog
# ------------


class Mailer(QtWidgets.QMainWindow):

    contactFile = None
    contactList = ()
    emails = ()
    attachments = []

    def __init__(self, title='Mail Dispatcher'):
        super(Mailer, self).__init__()
        uic.loadUi('UI/MailMainWindow.ui', self)
        self.title = title
        self.setWindowTitle(self.title)

        # * Setting an icon for the app
        if sys.platform == 'win32':
            appId = 'mailer'
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(appId)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap('res/icon.ico'))
        self.setWindowIcon(icon)
        self.workThread = QtCore.QThread()

        # * Activate buttons
        self.pushButtonSend.clicked.connect(self.onClickSend)
        self.pushButtonAddAttachments.clicked.connect(self.onClickAddAttachments)
        self.pushButtonRemoveAttachments.clicked.connect(self.onClickRemoveAttachments)
        self.pushButtonClearAttachments.clicked.connect(self.onClickClearAttachments)
        self.pushButtonEditList.clicked.connect(self.onClickEditList)

        # * Text formating options
        self.tabWidgetContent.currentChanged.connect(self.onChangeTabContent)
        # if not sys.platform == 'win32':
        #    self.textEditContentPlain.cursorPositionChanged.connect(lambda: self.onChangingFont(0))
        self.fontComboBox.currentFontChanged.connect(lambda: self.onChangingFont(1))
        self.spinBoxFontSize.valueChanged.connect(lambda: self.onChangingFont(2))
        self.pushButtonBold.clicked.connect(self.onClickBold)
        self.pushButtonUnderline.clicked.connect(self.onClickUnderline)
        self.pushButtonItalic.clicked.connect(self.onClickItalic)
        self.pushButtonMarkLink.clicked.connect(self.onClickMarkAsLink)
        self.pushButtonUnmarkLink.clicked.connect(self.onClickUnmarkLink)

        self.pushButtonTextColor.clicked.connect(self.conClickTextColor)

        # * Set up table for Attachments
        self.tableWidgetAttachments.itemSelectionChanged.connect(self.updateButtonStatus)
        self.pushButtonRemoveAttachments.setDisabled(True)
        self.tableWidgetAttachments.setColumnCount(2)
        self.tableWidgetAttachments.setHorizontalHeaderLabels(['File', 'Size'])
        self.updateAttachments()

        self.textEditContentPlain.customContextMenuRequested.connect(self.generateContextMenu)

        # * Activate actions
        # ? Menu File
        self.actionPreferences.triggered.connect(self.onClickPreferences)
        self.actionReload.triggered.connect(self.onClickReload)
        self.actionExit.triggered.connect(self.onClickExit)

        # ? Menu Mail
        self.actionClearMail.triggered.connect(self.onClickClearMail)
        self.actionOpenMail.triggered.connect(self.onClickOpenMail)
        self.actionSaveMail.triggered.connect(self.onClickSaveMail)
        self.actionSaveMailAs.triggered.connect(self.onClickSaveMailAs)
        self.actionOpenMailingList.triggered.connect(self.onClickOpenMailingList)
        self.actionSaveMailingList.triggered.connect(self.onClickSaveMailingList)

        # ? Menu Contacts
        self.actionNewContactList.triggered.connect(self.onClickNewContactList)
        self.actionEditContactList.triggered.connect(self.onClickEditContactList)
        self.actionImportContacts.triggered.connect(self.onClickImport)
        self.actionExportContacts.triggered.connect(self.onClickExport)

        # * Load configuration settings
        configs = PreferencesManager.loadConfiguration("res/config.ini")
        self.contactFile = configs[0]
        self.mailFile = configs[1]
        data = MailManager.readMail(self.mailFile)
        if '' in data:
            self.statusbar.showMessage('Mail {} file corrupted. Open another one.'.format(self.mailFile), 2000)
            self.mailFile = ''
        else:
            self.lineEditFrom.setText(data[0])
            self.emails = data[1]
            self.lineEditSubject.setText(data[2])
            self.textEditContentPlain.setHtml(data[3])
            self.textEditContentHtml.setText(data[3])
            self.onClickClearAttachments()
            self.addAttachments(data[4])
            self.mailChanged = False
            self.textEditContentPlain.textChanged.connect(self.onMailChanged)

        self.updateRecipients()
        if os.path.exists(self.contactFile):
            cManager = ContactManager(self.contactFile)
            self.contactList = cManager.importContacts()
            self.labelContactList.setText(
                self.contactFile.replace('\\', '/').split('/')[-1])

        self.show()

    def onClickSend(self):
        sender = self.lineEditFrom.text()
        recipient = self.textEditTo.toPlainText()
        subject = self.lineEditSubject.text()
        content = self.textEditContentPlain.toHtml()
        key = ''
        with open('res/api-key.mdak', 'r') as file:
            key = file.readlines()[0]

        if key == '':
            self.statusbar.showMessage('Unable to send mails. Enter the API-key of you Service provider into "api-key.mdak"', 2000)
            return
        if sender == '':
            self.statusbar.showMessage('Missing Sender!', 2000)
            messageBox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "No sender given!", 'Please secify a mail adress as the sender!')
            self.lineEditFrom.setStyleSheet('border: 1px solid red;')
            messageBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap('res/icon.ico'))
            messageBox.setWindowIcon(icon)
            messageBox.exec()
            return
        self.lineEditFrom.setStyleSheet('')

        if recipient == '':
            self.statusbar.showMessage('Missing Recipient!', 2000)
            messageBox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "No reciever(s) given!", 'Please specifiy, which persons you want to send this mail to!')
            self.textEditTo.setStyleSheet('border: 1px solid red;')
            messageBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap('res/icon.ico'))
            messageBox.setWindowIcon(icon)
            messageBox.exec()
            return
        self.textEditTo.setStyleSheet('')
        
        if subject == '':
            self.statusbar.showMessage('Missing Subject!', 2000)
            messageBox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "No subject given!", 'Please give the mail a topic!')
            self.lineEditSubject.setStyleSheet('border: 1px solid red;')
            messageBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap('res/icon.ico'))
            messageBox.setWindowIcon(icon)
            messageBox.exec()
            return
        self.lineEditSubject.setStyleSheet('')    

        if self.textEditContentPlain.toPlainText() == '':
            self.statusbar.showMessage('Missing Content!', 2000)
            messageBox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "No content given!", 'Please enter a message!')
            self.textEditContentPlain.setStyleSheet('border: 1px solid red;')
            messageBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap('res/icon.ico'))
            messageBox.setWindowIcon(icon)
            messageBox.exec()
            return
        self.textEditContentPlain.setStyleSheet('')

        self.statusbar.showMessage('Sending mail...')

        self.mailSender = MailManager.MailSender(sender, self.emails, subject, content, self.attachments)
        self.mailSender.moveToThread(self.workThread)
        self.mailSender.sendingFinished.connect(self.updateStatusbar)

        self.workThread.started.connect(self.mailSender.run)
        self.workThread.start()

    def updateStatusbar(self, status):
        if status.startswith('S'):
            self.statusbar.showMessage(status)
        elif not status.startswith('202'):
            self.statusbar.showMessage(
                'Sending Mail failed: ' + status[3:])
        else:
            self.statusbar.showMessage(status[3:])
            self.mailChanged = False

    def onClickAddAttachments(self, files=None):
        selectedFiles = QtWidgets.QFileDialog.getOpenFileNames(self, 'Add Attachments...')
        if not selectedFiles == '':
            self.addAttachments(selectedFiles[0])

    def addAttachments(self, files):
        for f in files:
            if f == '':
                continue
            encoded_file = None
            with open(f, 'rb') as file:
                data = file.read()
                encoded_file = base64.b64encode(data).decode()
                if os.stat(f).st_size > 10**7:
                    self.statusbar.showMessage('You can only add Files up to 10 MB as Attachment.', 2000)
                else:
                    self.attachments.append(Attachment(FileContent(encoded_file), FileName(f)) )

        self.updateAttachments()

    def onClickRemoveAttachments(self):
        items = self.tableWidgetAttachments.selectedItems()
        copy = self.attachments
        for i in items:
            for a in self.attachments:
                if i.text() == a.file_name.file_name.split('/')[-1]:
                    copy.remove(a)

        self.attachments = copy
        self.updateAttachments()

    def onClickClearAttachments(self):
        self.attachments = []
        self.tableWidgetAttachments.setRowCount(0)
        self.updateAttachments()

    def onClickEditList(self):
        if self.contactFile == None:
            self.statusbar.showMessage('No Contacts available', 2000)
            self.onClickImport()

        dialog = RecipientSelectDialog(
            self.contactList, self.contactFile, self.emails)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            cManager = ContactManager(self.contactFile)
            self.contactList = cManager.importContacts()

            self.emails = dialog.getMails()
            self.updateRecipients()
            self.labelMailingList.setText(
                self.labelMailingList.text() + ' (altered)')
    
    def onMailChanged(self):
        if not self.mailChanged:
            self.mailChanged = True
            self.setWindowTitle(self.title + '*')
            self.statusbar.showMessage('Mail changed.', 2000)

    def onCoursorChanged(self):
        font = self.textEditContentPlain.currentFont()
        self.fontComboBox.setCurrentFont(font)
        self.spinBoxFontSize.setValue(font.pointSize())
    
    def onSelectFont(self):
        font = self.fontComboBox.currentFont()
        
        currentFont = self.textEditContentPlain.currentFont()
        if currentFont.bold():
            font.setBold(True)
        if currentFont.italic():
            font.setItalic(True)
        if currentFont.underline():
            font.setUnderline(True)

        fontSize = currentFont.pointSize()
        font.setPointSize(fontSize)

        print(font.family())
        self.textEditContentPlain.setCurrentFont(font)
        
        self.fontComboBox.setCurrentFont(font)
    
    def onSelectPointSize(self):
        font = self.textEditContentPlain.currentFont()
        font.setPointSize(self.spinBoxFontSize.value())
        self.textEditContentPlain.setCurrentFont(font)

    def onChangeTabContent(self):
        if self.tabWidgetContent.currentIndex() == 1:
            htmlText = self.textEditContentPlain.toHtml()
            self.textEditContentHtml.setPlainText(htmlText)
        else:
            textPlain = self.textEditContentHtml.toPlainText()
            self.textEditContentPlain.setHtml(textPlain)

    def onChangingFont(self, mode):
        try:
            # if not sys.platform == 'win32':
            #    self.textEditContentPlain.cursorPositionChanged.disconnect()
            pass
        except:
            pass
        
        try:
            self.fontComboBox.currentFontChanged.disconnect()
        except:
            pass
            
        try:
            self.spinBoxFontSize.valueChanged.disconnect()
        except:
            pass

        if mode == 0:
            # self.onCoursorChanged()
            pass
        elif mode == 1:
            self.onSelectFont()
        elif mode == 2:
            self.onSelectPointSize()
        
        # if not sys.platform == 'win32':
        #     self.textEditContentPlain.cursorPositionChanged.connect(lambda: self.onChangingFont(0))
        self.fontComboBox.currentFontChanged.connect(lambda: self.onChangingFont(1))
        self.spinBoxFontSize.valueChanged.connect(lambda: self.onChangingFont(2))

    def onClickBold(self):
        font = self.textEditContentPlain.currentFont()

        if not font.bold():
            font.setBold(True)
        else:
            font.setBold(False)
            
        self.textEditContentPlain.setCurrentFont(font)

    def onClickItalic(self):
        font = self.textEditContentPlain.currentFont()
        if not font.italic():
            font.setItalic(True)
        else:
            font.setItalic(False)
        
        self.textEditContentPlain.setCurrentFont(font)

    def onClickUnderline(self):
        font = self.textEditContentPlain.currentFont()
        if not font.underline():
            font.setUnderline(True)
        else:
            font.setUnderline(False)
        self.textEditContentPlain.setCurrentFont(font)

    def onClickPreferences(self):
        dialog = PreferencesDialog()
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.statusbar.showMessage('Preferences Changed. Reload program for changes to take effect (File -> Reload Window / F5)', 3000)

    def onClickReload(self):
        self.textEditContentPlain.textChanged.disconnect(self.onMailChanged)
        configs = PreferencesManager.loadConfiguration("res/config.ini")
        data = MailManager.readMail(configs[1])
        self.lineEditFrom.setText(data[0])
        self.emails = data[1]
        self.lineEditSubject.setText(data[2])
        self.textEditContentPlain.setHtml(data[3])
        self.textEditContentHtml.setText(data[3])
        self.onClickClearAttachments()
        self.addAttachments(data[4])
        self.mailChanged = False
        self.textEditContentPlain.textChanged.connect(self.onMailChanged)

        self.updateRecipients()
        if os.path.exists(self.contactFile):
            cManager = ContactManager(self.contactFile)
            self.contactList = cManager.importContacts()
        self.statusbar.showMessage('Data reinitialized, program reloaded', 2000)

    def onClickExit(self):
        self.close()

    def onClickClearMail(self):
        self.mailFile = ''
        self.textEditContentPlain.textChanged.disconnect(self.onMailChanged)
        self.lineEditFrom.setText('')
        self.emails = ()
        self.updateRecipients()
        self.lineEditSubject.setText('')
        self.textEditContentPlain.setText('')
        self.mailChanged = False
        self.textEditContentPlain.textChanged.connect(self.onMailChanged)
        self.statusbar.showMessage('All data from mail cleared', 2000)
        self.onClickClearAttachments()

    def onClickOpenMail(self):
        self.mailFile = QtWidgets.QFileDialog.getOpenFileName(
            self, 'Open Mail', filter="MDMT (*.mdmt)")[0]
        if not self.mailFile == '':
            try:
                self.textEditContentPlain.textChanged.disconnect()
            except:
                pass
            data = MailManager.readMail(self.mailFile)
            self.lineEditFrom.setText(data[0])
            self.emails = data[1]
            self.updateRecipients()
            self.lineEditSubject.setText(data[2])
            self.textEditContentPlain.setHtml(data[3])
            self.textEditContentHtml.setText(data[3])
            self.onClickClearAttachments()
            self.addAttachments(data[4])
            self.mailChanged = False
            self.textEditContentPlain.textChanged.connect(self.onMailChanged)
            self.statusbar.showMessage('Opened mail "' + self.mailFile.split('/')[-1] + '"!', 2000)

    def onClickSaveMail(self):
        textFrom = self.lineEditFrom.text()
        textTo = str(self.emails).replace('[', '').replace(']', '').replace(
            '(', '').replace(')', '').replace('\'', '').replace(',', ';').replace(' ', '')
        textSubject = self.lineEditSubject.text()
        attachments = ''
        for a in self.attachments:
            attachments += a.file_name.file_name + ';'
        textContent = self.textEditContentPlain.toHtml()
        if self.tabWidgetContent.currentIndex() == 1:
            textContent = self.textEditContentHtml.toPlainText()

        if self.mailFile == '':
            self.mailFile = QtWidgets.QFileDialog.getSaveFileName(self, 'Save Mail As...', filter="MDMT (*.mdmt)")[0]
        
        if not self.mailFile == '':
            MailManager.saveMail(self.mailFile, textFrom, textTo, textSubject, attachments, textContent)
            self.statusbar.showMessage('Saved Mail as"' + self.mailFile + '"!', 2000)
            self.mailChanged = False
            self.setWindowTitle(self.title)
    
    def onClickSaveMailAs(self):
        self.mailFile = ''
        self.onClickSaveMail()

    def onClickOpenMailingList(self):
        mailFile = QtWidgets.QFileDialog.getOpenFileName(
            self, 'Open Mailing List', filter="MDML (*.mdml)")[0]
        if not mailFile == '':
            with open(mailFile, 'r') as file:
                data = file.readlines()
                if len(data) > 1:
                    return
                else:
                    emails = data[0].split(';')
                    available = ()
                    contactMails = [
                        contact.email for contact in self.contactList]
                    for e in emails:
                        if e in contactMails:
                            available += (e, )
                        else:
                            newContact = Contact(email=e)
                            cm = ContactManager(self.contactFile)
                            cm.saveContact(newContact)
                            self.contactList += (newContact, )
                            available += (e, )

                    self.emails = available
                    self.updateRecipients()
                    self.labelMailingList.setText(
                        mailFile.replace('\\', '/').split('/')[-1])
            self.statusbar.showMessage('Opened Mailing List "' + mailFile.split('/')[-1] + '"!', 2000)

    def onClickSaveMailingList(self):
        mailFile = QtWidgets.QFileDialog.getSaveFileName(
            self, 'Save Mailing List', filter="MDML (*.mdml)")[0]
        if not mailFile == '':
            with open(mailFile, 'w') as file:
                file.write(str(self.emails).replace('[', '').replace(']', '').replace(
                    '(', '').replace(')', '').replace('\'', '').replace(',', ';').replace(' ', ''))
            self.labelMailingList.setText(
                mailFile.replace('\\', '/').split('/')[-1])
            self.statusbar.showMessage('Saved Mailing List as "' + mailFile + '"!', 2000)

    def onClickNewContactList(self):
        mailFile = QtWidgets.QFileDialog.getSaveFileName(
            self, 'Save Mailing List', filter="CSV (*.csv)")[0]
        if not mailFile == '':
            with open(mailFile, 'w') as file:
                file.write('')
            self.contactFile = mailFile
            self.labelContactList.setText(
                self.contactFile.replace('\\', '/').split('/')[-1])
            self.contactList = ()
            self.emails = ()
            self.updateRecipients()

    def onClickEditContactList(self):
        dialog = ContactListDialog(self.contactList, self.contactFile)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.statusbar.showMessage('Contacts edited.', 5000)
        self.contactList = dialog.getContactList()

    def onClickImport(self):
        self.contactFile = QtWidgets.QFileDialog.getOpenFileName(
            self, 'Import Contacts', filter="CSV (*.csv)")[0]
        if not self.contactFile == '':
            cManager = ContactManager(self.contactFile)
            self.contactList = cManager.importContacts()
            newMails = ()
            for c in self.contactList:
                if c.email in self.emails:
                    newMails += (c.email, )
            self.emails = newMails
            self.updateRecipients()
            self.statusbar.showMessage(str(len(self.contactList)) + ' Contacts imported from "' +
                                       self.contactFile.replace('\\', '/').split('/')[-1] + '"!', 5000)
            self.labelContactList.setText(
                self.contactFile.replace('\\', '/').split('/')[-1])

    def onClickExport(self):
        exportFile = QtWidgets.QFileDialog.getSaveFileName(
            self, 'Export Contacts', filter="CSV (*.csv)")[0]
        if not exportFile == '':
            cManager = ContactManager(exportFile)
            status = cManager.exportContacts(self.contactList)
            if not status == None:
                self.statusbar.showMessage(str(len(
                    self.contactList)) + ' Contacts exported to "' + exportFile.replace('\\', '/').split('/')[-1] + '"!', 5000)
                self.contactFile = exportFile
                self.labelContactList.setText(
                    exportFile.replace('\\', '/').split('/')[-1])
            else:
                self.statusbar.showMessage(
                    'An error occured while exporting Contacts!', 5000)

    def onClickUsage(self):
        print()

    def onClickInformation(self):
        print()

    def onClickMarkAsLink(self):
        cursor = self.textEditContentPlain.textCursor()
        text = cursor.selectedText()
        self.textEditContentPlain.cut()
        htmlText = '<a href={}>{}</a>'.format(text.replace('https://', '').replace('http://', ''), text)
        self.textEditContentPlain.insertHtml(htmlText)

    def onClickUnmarkLink(self):
        text = self.textEditContentPlain.textCursor().selectedText()
        self.textEditContentPlain.cut()
        self.textEditContentPlain.insertPlainText(text)

    def conClickTextColor(self):
        dialog = QtWidgets.QColorDialog()
        if dialog.exec() == 1:
            color = dialog.selectedColor()
            #font = self.textEditContentPlain.currentFont()
            # cursor = self.textEditContentPlain.textCursor()
            # text = self.textEditContentPlain.textCursor().selectedText()
            # self.textEditContentPlain.cut()
            # cursor.color(color)
            # cursor.insertText(text)
            self.textEditContentPlain.setTextColor(color)

    def updateAttachments(self):
        self.tableWidgetAttachments.setRowCount(0)
        self.tableWidgetAttachments.setRowCount(len(self.attachments))
        n = 0
        for a in self.attachments:
            item = QtWidgets.QTableWidgetItem(
                a.file_name.file_name.split('/')[-1])
            self.tableWidgetAttachments.setItem(n, 0, item)
            size = self.convertSize(os.stat(a.file_name.file_name).st_size)
            item = QtWidgets.QTableWidgetItem(size)
            self.tableWidgetAttachments.setItem(n, 1, item)
            n += 1

        header = self.tableWidgetAttachments.horizontalHeader()
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)

        self.labelAttachmentCount.setText(str(len(self.attachments)))
        size = 0
        for a in self.attachments:
            size += os.stat(a.file_name.file_name).st_size
        self.labelAttachmentSize.setText(self.convertSize(size))

    def updateRecipients(self):
        self.textEditTo.setText(str(self.emails).replace('(', '').
                                replace(')', '').
                                replace('[', '').
                                replace(']', '').
                                replace('\'', '').
                                replace(',', '\n').
                                replace(';', '\n').
                                replace(' ', ''))
        self.labelRecipientCount.setText(str(len(self.emails)))

    def updateButtonStatus(self):
        if len(self.tableWidgetAttachments.selectedItems()) == 0:
            self.pushButtonRemoveAttachments.setDisabled(True)
        else:
            self.pushButtonRemoveAttachments.setDisabled(False)

    def convertSize(self, number):
        number = round(number/1000, 1)
        iter = 0
        while number > 999 and iter < 4:
            number = round(number/1000, 1)
            iter += 1

        if iter == 0:
            return str(number) + ' KB'
        elif iter == 1:
            return str(number) + ' MB'
        elif iter == 2:
            return str(number) + ' GB'

        return str(number) + ' TB'

    def generateContextMenu(self):
        menu = self.textEditContentPlain.createStandardContextMenu()
        menu.addSeparator()

        self.actionMarkAsLink = QtWidgets.QAction('Mark as Link')
        self.actionMarkAsLink.triggered.connect(self.onClickMarkAsLink)
        self.actionUnmarkLink = QtWidgets.QAction('Unmark Link')
        self.actionUnmarkLink.triggered.connect(self.onClickUnmarkLink)
        menu.addAction(self.actionMarkAsLink)
        menu.addAction(self.actionUnmarkLink)

        menu.exec(QtGui.QCursor.pos())


if __name__ == '__main__':
    if not os.path.exists('res/config.ini'):
        file = open('res/config.ini', 'w')
        file.write('')
        file.close()
        with open('res/config.ini', 'w') as file:
            file.write("Configuration for Mailer:\n" +
                       "PathToContactlist = res/Adressbuch.csv\n" +
                       "StandartMail      = ")
    app = QtWidgets.QApplication(sys.argv)
    window = Mailer()
    sys.exit(app.exec_())
