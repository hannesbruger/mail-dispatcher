import csv
from Contact import Contact

class ContactManager():
    def __init__(self, file):
        self.filename = file
        self.contactList = ()

    def importContacts(self):
        if self.filename == None:
            return ()
        
        with open(self.filename, 'r', newline='') as contactfile:
            reader = csv.reader(contactfile)
            contacts = ()
            mails = ()
            for row in reader:
                if not row == '':
                    fname = row[0]
                    lname = row[1]
                    email = row[4]
                    if not email in mails and '@' in email:
                        contact = Contact(fname, lname, email)
                        contacts += (contact, )
                        mails += (email, )

            return contacts
        return None
    
    def exportContacts(self, contactList):
        if self.filename == None:
            return
        
        with open(self.filename, 'w', newline='') as contactfile:
            writer = csv.writer(contactfile)
            writtenMails = ()
            for contact in contactList:
                if not contact.email in writtenMails and '@' in contact.email:
                    writer.writerow([contact.firstName, contact.lastName, contact.name, '',contact.email])
                    writtenMails += (contact.email, )
            return self.filename
        return None

    def saveContact(self, contact):
        with open(self.filename, 'a', newline='') as contactfile:
            writer = csv.writer(contactfile)
            writer.writerow([contact.firstName, contact.lastName, contact.name, '',contact.email])
            return self.filename
        return None

if __name__ == '__main__':
    cm = ContactManager("Adressbuch.csv")
    list = cm.importContacts()
    for c in list:
        print(c.name, c.email, sep=': ')