import ctypes
from sys import platform
from math import ceil

from PyQt5 import QtCore, QtGui, QtWidgets, uic

from Contact import Contact
from ContactManager import ContactManager


class RecipientSelectDialog(QtWidgets.QDialog):
    def __init__(self, contactlist, contactFile, selectedContacts=None):
        super(RecipientSelectDialog, self).__init__()
        uic.loadUi('UI/RecipientSelectDialog.ui', self)
        self.setWindowTitle('Select recipients...')
        self.contactFile = contactFile

        if platform == 'win32':
            appId = 'mailer'
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
                appId)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap('res/icon.ico'))
        self.setWindowIcon(icon)

        self.tableWidget.itemSelectionChanged.connect(self.updateLabelCount)

        self.pushButtonAdd.clicked.connect(self.onClickAdd)
        self.pushButtonSet.clicked.connect(self.accept)
        self.pushButtonCancel.clicked.connect(self.reject)
        self.pushButtonSelectAll.clicked.connect(self.onClickSelectAll)
        self.pushButtonDeselectAll.clicked.connect(self.onClickDeselectAll)
        self.pushButtonInvertSelection.clicked.connect(
            self.onClickInvertSelection)

        self.contactList = contactlist
        self.selectedContacts = selectedContacts

        self.tableWidget.setColumnCount(3)
        self.tableWidget.setHorizontalHeaderLabels(
            ['First name', 'Last name', 'Email'])
        header = self.tableWidget.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)
        self.updateTable(self.contactList)

        # if len(self.contactList) == 0:
        #    self.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setDisabled(True)

        self.show()

    def updateLineEdits(self):
        items = self.tableWidget.selectedItems()[:3]
        if len(items) >= 3:
            self.lineEditFirstName.setText(items[0].text())
            self.lineEditLastName.setText(items[1].text())
            self.lineEditMail.setText(items[2].text())

    def updateTable(self, contactlist):
        self.tableWidget.clear()
        self.tableWidget.setRowCount(len(contactlist))
        selected = ()
        n = 0
        for contact in contactlist:
            item1 = QtWidgets.QTableWidgetItem(contact.firstName)
            self.tableWidget.setItem(n, 0, item1)
            item2 = QtWidgets.QTableWidgetItem(contact.lastName)
            self.tableWidget.setItem(n, 1, item2)
            item3 = QtWidgets.QTableWidgetItem(contact.email)
            self.tableWidget.setItem(n, 2, item3)
            if contact.email in self.selectedContacts:
                selected += (item1, )
                selected += (item2, )
                selected += (item3, )
            n += 1

        for item in selected:
            item.setSelected(True)
        self.updateLabelCount()

        self.tableWidget.sortItems(2)

    def updateLabelCount(self):
        selected = self.tableWidget.selectedItems()
        self.labelCount.setText(str(int(ceil(len(selected)/3))))

    def selectContact(self, contact):
        item = self.tableWidget.findItems(
            contact.email, QtCore.Qt.MatchExactly)[0]
        self.tableWidget.scrollToItem(
            item, hint=QtWidgets.QAbstractItemView.PositionAtCenter)
        item.setSelected(True)

    def onClickSelectAll(self):
        self.tableWidget.selectAll()

    def onClickDeselectAll(self):
        selectedItems = self.tableWidget.selectedItems()
        for item in selectedItems:
            item.setSelected(False)

    def onClickInvertSelection(self):
        selectedItems = self.tableWidget.selectedItems()
        self.tableWidget.selectAll()
        for item in selectedItems:
            item.setSelected(False)

    def onClickAdd(self):
        if self.lineEditMail.text() == '':
            self.lineEditMail.setStyleSheet('background-color: #FFDDDD')
            return
        self.lineEditMail.setStyleSheet('background-color: #FFFFFF')

        firstName = self.lineEditFirstName.text()
        lastName = self.lineEditLastName.text()
        email = self.lineEditMail.text()

        contact = Contact(firstName, lastName, email)
        exists = False
        for c in self.contactList:
            if contact.equals(c):
                exists = True
        if not exists:
            self.contactList += (contact, )
            self.updateTable(self.contactList)
            self.selectContact(contact)

            cmgr = ContactManager(self.contactFile)
            cmgr.saveContact(contact)

            self.lineEditFirstName.setText('')
            self.lineEditLastName.setText('')
            self.lineEditMail.setText('')
        else:
            self.lineEditMail.setStyleSheet('background-color: #FFDDDD')
            messageBox = QtWidgets.QMessageBox(
                QtWidgets.QMessageBox.Warning, '', 'There is already a contact with this Email!')
            messageBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
            messageBox.exec()

    def getMails(self):
        items = self.tableWidget.selectedItems()
        mails = ()
        for i in items:
            if '@' in i.text():
                mails += (i.text(), )
        return mails
