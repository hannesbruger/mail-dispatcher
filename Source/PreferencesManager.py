def loadConfiguration(fileName):
    result = ['', '']
    with open(fileName, 'r') as file:
        data = file.readlines()
        for line in data:
            if line.startswith('Path'):
                result[0] = line.split('=')[1].strip()
            
            elif line.startswith('StandartMail'):
                result[1] = line.split('=')[1].strip()

    return result


def saveConfiguration(fileName, contactPath, mailPath, key):
    with open(fileName, 'w') as file:
        file.write('Configuration for Mailer:' +
                   '\nPathToContactlist  = ' + contactPath +
                   '\nStandartMail       = ' + mailPath)
    
    with open('res/api-key.mdak', 'w') as file:
        file.write(key)
