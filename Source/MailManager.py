import os

from PyQt5 import QtCore

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail, Attachment, FileContent, FileName, FileType, Disposition

class MailSender(QtCore.QObject):

    sendingFinished = QtCore.pyqtSignal(str)
    status_code = 0

    def __init__(self, sender, recievers, subject, content, attachments=()):
        super(MailSender, self).__init__()
        self.sender = sender
        self.receivers = recievers
        self.subject = subject
        self.content = content
        self.attachments = attachments
        self.key = ''
        with open('res/api-key.mdak') as file:
            self.key = file.readlines()[0]

    def run(self):
        if self.key == '':
            self.sendingFinished.emit('000No API-key found.')
        personCount = 0
        for r in self.receivers:
            try:
                self.sendingFinished.emit('Sending mail to ' + r + '...')
                self.sendMail(r)
                personCount += 1
            except Exception as e:
                print(e)
                self.sendingFinished.emit('000' + str(e))
                break

        if not personCount == len(self.receivers):
            self.sendingFinished.emit(
                '000Couldn\'t reach all recievers (status ' + str(self.status_code) + ')')
        else:
            self.sendingFinished.emit(
                str(self.status_code) + 'Mail sent to ' + str(personCount) + ' persons! (status: ' + str(self.status_code) + ')')

    def sendMail(self, recipient):
        message = Mail(from_email=self.sender, to_emails=recipient,
                       subject=self.subject, html_content=self.content)
        clearAttachments = self.attachments
        for a in clearAttachments:
            a.file_name.file_name = a.file_name.file_name.split('/')[-1]
        message.attachment = clearAttachments

        try:
            sg = SendGridAPIClient(self.key)
            response = sg.send(message)
            self.status_code = response.status_code
            # print(response.status_code)
            # print(response.headers)
            # print(response.body)
        except Exception as e:
            self.status_code = response.status_code
            raise e

def readMail(fileName):
    result = ['', '', '', '', '']
    if not os.path.exists(fileName):
        return result
    with open(fileName, 'r') as file:
        data = file.readlines()
        readHtml = False
        content = ''
        for line in data:
            if line.startswith('From'):
                result[0] = line.split('=')[1].strip()
            elif line.startswith('To'):
                result[1] = line.split('=')[1].strip().split(';')
            elif line.startswith('Subject'):
                result[2] = line.split('=')[1].strip()
            elif line.startswith('Attachments'):
                result[4] = line.split('=')[1].strip().split(';')
            elif line.startswith('Content'):
                readHtml = True
            elif readHtml:
                if not line.startswith('---'):
                    content += line
                else:
                    readHtml = False

        result[3] = content
    
    return result

def saveMail(mailFile, textFrom, textTo, textSubject, attachments, textContent):
    with open(mailFile, 'w') as file:
        file.write('From    = ' + textFrom + '\n')
        file.write('To      = ' + textTo + '\n')
        file.write('Subject = ' + textSubject + '\n')
        file.write('Attachments= ' + attachments + '\n')
        file.write('Content =\n' + textContent + '\n---')