import ctypes
from sys import platform

from PyQt5 import QtCore, QtGui, QtWidgets, uic

from RecipientSelectDialog import RecipientSelectDialog
from ContactManager import ContactManager
import PreferencesManager as PreferencesManager


class PreferencesDialog(QtWidgets.QDialog):
    def __init__(self):
        super(PreferencesDialog, self).__init__()
        uic.loadUi('UI/PreferencesDialog.ui', self)
        self.setWindowTitle('Change Preferences')

        if platform == 'win32':
            appId = 'mailer'
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
                appId)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap('res/icon.ico'))

        self.pushButtonOpenContacts.clicked.connect(self.onClickOpenContacts)
        self.pushButtonOpenMail.clicked.connect(self.onClickOpenMail)
        self.buttonBox.accepted.connect(self.save)
        self.buttonBox.rejected.connect(self.reject)

        self.setWindowIcon(icon)

        configs = PreferencesManager.loadConfiguration("res/config.ini")
        self.contactFile = configs[0]

        self.lineEditPath.setText(configs[0])
        self.lineEditMail.setText(configs[1])

        self.key = ''
        with open('res/api-key.mdak') as file:
            self.key = file.readlines()[0]

        self.show()

    def onClickOpenContacts(self):
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, 'Import Contacts', filter="CSV (*.csv)")[0]
        if not fileName == '':
            self.lineEditPath.setText(fileName)
        
    def onClickOpenMail(self):
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, 'Open Mail', filter="MDMT (*.mdmt)")[0]
        if not fileName == '':
            self.lineEditMail.setText(fileName)

    def save(self):
        PreferencesManager.saveConfiguration("res/config.ini",
                                             self.lineEditPath.text(),
                                             self.lineEditMail.text(),
                                             self.key)
        self.done(1)

    def generateContextMenu(self):
        menu = self.textEditMessage.createStandardContextMenu()
        menu.addSeparator()

        self.actionMarkAsLink = QtWidgets.QAction('Mark as Link')
        self.actionMarkAsLink.triggered.connect(self.OnClickMarkAsLink)
        menu.addAction(self.actionMarkAsLink)

        menu.exec(QtGui.QCursor.pos())
